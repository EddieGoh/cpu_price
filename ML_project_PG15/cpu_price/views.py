#from __future__ import unicode_literals
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
import logging
from django.http import JsonResponse
from sklearn.metrics import mean_squared_error, r2_score
import numpy as np
import pylab as pl
import pandas as pd
import csv
from csv import reader
from sklearn import datasets, linear_model, neighbors
import os
import warnings
from sklearn.model_selection import train_test_split

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

logger = logging.getLogger(__name__)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

def index(request):
    return render(request, "index.html")

# Load a CSV file
def load_csv(filename):
    file = open(filename, "rU")
    lines = reader(file)
    dataset = list(lines)
    return dataset

# Convert string column to float
def str_column_to_float(dataset, column):
    for row in dataset:
        row[column] = float(row[column].strip())

def getFileData(request):

    if request.method == 'GET':

        filePath = request.GET.get('path')
        print "filePath = " + filePath

        fullFilePath = BASE_DIR + filePath
        fileContent = load_csv(fullFilePath)
        # print fileContent

        partialContent = fileContent[0:5]

        response = JsonResponse(partialContent, safe=False)

        return HttpResponse(response, status=200)


def train(request):

    if request.method == 'GET':

        return HttpResponse("GET /train", status=200)

    elif request.method == 'POST':

        # user input parameters
        dataX = request.POST.get('data_x_url','')
        dataY = request.POST.get('target_y_url','')

        dataXFullPath = BASE_DIR + dataX
        dataYFullPath = BASE_DIR + dataY

        # print "dataXFullPath = " + dataXFullPath
        # print "dataYFullPath = " + dataYFullPath

        def isnumber(s):
            try:
                float(s)
                return True
            except ValueError:
                return False

        # TODO: call script to perform computation here...
	    # Load a CSV file
        def load_csv(filename):
            file = open(filename, "rU")
            lines = reader(file)
            dataset = list(lines)

            if (isnumber(dataset[0][0])):
                return dataset
            else:
                return dataset[1:]

        # Convert string column to float
        def str_column_to_float(dataset, column):
            for row in dataset:
                row[column] = float(row[column].strip())

        def read_lines():
            with open(dataXFullPath, 'rU') as data:
                data_x = csv.reader(data, delimiter=",")

                for row in data_x:
                    if (isnumber(row[0])):
                        yield [float(i) for i in row]

        # get cpu_data_array
        cpu_data_array = []
        for i in read_lines():
            cpu_data_array.extend([i])

        # print cpu_data_array
        # for i in cpu_data_array:
        #     print i

        #get cpu_price_array
        cpu_price_array = load_csv(dataYFullPath)
        for j in range(len(cpu_price_array[0])):
            str_column_to_float(cpu_price_array, j)

        #print cpu_price_array
        # for i in cpu_price_array:
        #     print i

        x = cpu_data_array
        y = cpu_price_array

        # Split dataset into 70:30 for train_set and test_test
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state = 42)
        print x_train, x_test, y_train, y_test

        # Create linear regression object
        knn = neighbors.KNeighborsClassifier(n_neighbors=1, weights='uniform',algorithm='auto')

        # Train the model using the training sets
        knn.fit(x_train, y_train)
        p = knn.predict(x_test)

        # Test R-square score for metrics
        knn_accuracy = (r2_score(y_test, p))

        # response with the result
        result = knn_accuracy
        response = JsonResponse(result.tolist(), safe=False)

        return response


def predict(request):

    if request.method == 'GET':

        return HttpResponse("GET /predict", status=200)

    elif request.method == 'POST':

        # user input parameters
        dataX = request.POST.get('data_x_url','')
        dataY = request.POST.get('target_y_url','')
        featuresString = request.POST.get('features','')

        def toFloat(string):
            floatValue = float(string)
            return floatValue

        features = featuresString.split(',')
        features = map(toFloat, features)

        dataXFullPath = BASE_DIR + dataX
        dataYFullPath = BASE_DIR + dataY

        # print "dataXFullPath = " + dataXFullPath
        # print "dataYFullPath = " + dataYFullPath
        print features

        def isnumber(s):
            try:
                float(s)
                return True
            except ValueError:
                return False

        # TODO: call script to perform computation here...
        # Load a CSV file
        def load_csv(filename):
            file = open(filename, "rU")
            lines = reader(file)
            dataset = list(lines)
            if (isnumber(dataset[0][0])):
                return dataset
            else:
                return dataset[1:]

        # Convert string column to float
        def str_column_to_float(dataset, column):
            for row in dataset:
                row[column] = float(row[column].strip())

        def read_lines():
            with open(dataXFullPath, 'rU') as data:
                data_x = csv.reader(data, delimiter=",")
                for row in data_x:
                    if (isnumber(row[0])):
                       yield [float(i) for i in row]

        # get cpu_data_array
        cpu_data_array = []
        for i in read_lines():
            cpu_data_array.extend([i])

        # print cpu_data_array
        # for i in cpu_data_array:
            # print i

        # get cpu_price_array
        cpu_price_array = load_csv(dataYFullPath)
        #next(cpu_price_array)
        for j in range(len(cpu_price_array[0])):
            str_column_to_float(cpu_price_array, j)

        # print cpu_price_array
        # for i in cpu_price_array:
            # print i

        x = cpu_data_array
        y = cpu_price_array

        # Split dataset into 70:30 for train_set and test_test
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state = 42)
        print x_train, x_test, y_train, y_test

        # Create linear regression object
        knn = neighbors.KNeighborsClassifier(n_neighbors=1, weights='uniform',algorithm='auto')

        # Train the model using the training sets
        knn.fit(x_train, y_train)

        # Compute predict price with features
        predict_price_array = knn.predict(np.dot([features],1))
        for pprice in predict_price_array:
            print pprice

        # response with the result
        result = pprice
        response = JsonResponse(result, safe=False)

        return response
